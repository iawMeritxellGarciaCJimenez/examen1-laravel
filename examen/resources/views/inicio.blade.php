<!DOCTYPE html>
<meta charset="utf-8">
<html lang="en" class="h-100">
    <head>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Hugo 0.88.1">
        <title>Welcome</title>
        
    </head>
    <body class="d-flex h-100 text-center text-white bg-dark">
        <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
            <header class="mb-auto">
              <div>
                <h3 class="float-md-start mb-0">Welcome</h3>
                
              </div>
            </header>
          
            <main class="px-3">
              <h1>Welcome</h1>
              <p class="lead">Puedes pulsar este boton para ver los productos</p>
              <p class="lead">
                <a href="protuctos/view" class="btn btn-lg btn-secondary fw-bold border-white bg-black">Productos</a>
              </p>
            </main>
          
            <footer class="mt-auto text-white-50">
              <p>From Meritxell &copy; 2021</p>
            </footer>
          </div>
    </body>
</html>
