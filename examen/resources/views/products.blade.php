<!DOCTYPE html>
<meta charset="utf-8">
<html lang="en" class="h-100">
    <head>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

      
    </head>
    <body class="d-flex h-100 text-center text-white bg-dark">
        <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
            <header class="mb-auto">
              <div>
                <h3 class="float-md-start mb-0">Productos</h3>
                
              </div>
            </header>
          
            <main class="px-3">
                <table class="table text-white">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">nombre</th>
                        <th scope="col">precio</th>
                        <th scope="col">descripción</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Manzana</td>
                        <td>10€</td>
                        <td>Es una fruta</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Pechuga</td>
                        <td>30€</td>
                        <td>Es carne de pollo</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Tomate</td>
                        <td>2€</td>
                        <td>Es una verdura</td>
                    </tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">nombre</th>
                        <th scope="col">precio</th>
                        <th scope="col">descripción</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Manzana</td>
                        <td>10€</td>
                        <td>Es una fruta</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Pechuga</td>
                        <td>30€</td>
                        <td>Es carne de pollo</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Tomate</td>
                        <td>2€</td>
                        <td>Es una verdura</td>
                    </tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">nombre</th>
                        <th scope="col">precio</th>
                        <th scope="col">descripción</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Manzana</td>
                        <td>10€</td>
                        <td>Es una fruta</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Pechuga</td>
                        <td>30€</td>
                        <td>Es carne de pollo</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Tomate</td>
                        <td>2€</td>
                        <td>Es una verdura</td>
                    </tr>
                    </tbody>
                </table>
            </main>
          
            <footer class="mt-auto text-white-50">
              <p>From Meritxell &copy; 2021</p>
            </footer>
          </div>
    </body>
</html>